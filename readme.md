## USAGE:
```
const sms = new SMSru({
  apiId: 'YOUR-API-ID',
  test: true
})

sms
  .status('201818-1000001')
  .then(result => {
    console.log(result)
  }).catch(err => {
    console.log(err)
  })

sms.send({
  to: ['79001234567'],
  msg: 'Ваш код: 5522',
  // test: true
}).then(result => {
  console.log(result)
}).catch(err => {
  console.log(err)
})

sms.cost({
  to: ['79001234567','79007654321'],
  msg: 'Привет! Это проверка.'
}).then(result => {
  console.log(result)
}).catch(err => {
  console.log(err)
})

sms.senders()
.then(result => {
  console.log(result)
}).catch(err => {
  console.log(err)
})

sms.send({
  multi: [
    { to: '79001234567', msg: 'Тест!' },
    { to: '79007654321', msg: 'Еще один тест!' }
  ],
  test: true
})
.then(result => {
  console.log(result)
}).catch(err => {
  console.log(err)
})

```
