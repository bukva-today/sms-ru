const https = require('https')
const qs = require('querystring')

const API_URL = 'https://sms.ru/'

class SMSru {

  constructor({ apiId, login, password, test = false }) {
    this.apiId = apiId
    this.login = login
    this.password = password
    this.test = test
  }

  /**
   * Отправка SMS
   *
   * @param {String|String[]} to
   * @param {String} msg
   * @param {String} from
   * @param {Date|Number} time
   * @param {Boolean} translit
   * @param {Boolean} test
   * @param {Number} partnerId
   * @param {Object[]} partnerId
   *
   * @return {Promise}
   */
  send({to, msg, from, time, translit = false, test = false, partnerId, multi}) {
    let params = arguments[0]

    if (Array.isArray(multi)) {
      delete params.multi
      multi.forEach((sms) => {
        params[`to[${sms.to}]`] = sms.msg
      })
    }

    if (Array.isArray(to)) {
      params.to = to.join(',')
    }

    if (time && time instanceof Date) {
      params.time = time.getTime() / 1000
    }

    if (translit) {
      params.translit = 1
    }

    if (this.test || test) {
      params.test = 1
    }

    return this.api('sms/send', params)
  }

  /**
   * Проверка статуса SMS
   *
   * @param {String} id
   *
   * @return {Promise}
   */
  status(id) {
    return this.api('sms/status', {id})
  }

  /**
   * Проверка стоимости отправки SMS
   *
   * @param {String|String[]} to
   * @param {String} msg
   * @param {String} from
   * @param {Boolean} translit
   *
   * @return {Promise}
   */
  cost({to, msg, from, translit = false}) {
    let params = arguments[0]

    if (Array.isArray(to)) {
      params.to = to.join(',')
    }

    return this.api('sms/cost', params)
  }

  /**
   * Запрос баланса
   *
   * @return {Promise}
   */
  balance() {
    return this.api('my/balance')
  }

  /**
   * Запрос лимита на отправку
   *
   * @return {Promise}
   */
  limit() {
    return this.api('my/limit')
  }

  /**
   * Запрос остатка бесплатных сообщений
   *
   * @return {Promise}
   */
  free() {
    return this.api('my/free')
  }

  /**
   * Запрос списка одобренных отправителей
   *
   * @return {Promise}
   */
  senders() {
    return this.api('my/senders')
  }

  /**
   * Проверить на валидность пару логин/пароль
   *
   * @return {Promise}
   */
  check() {
    return this.api('auth/check')
  }

  /**
   * Добавление номера в стоплист
   *
   * @param {String} phone
   * @param {String} comment
   *
   * @return {Promise}
   */
  stoplistAdd({phone, comment}) {
    let params = { stoplist_phone: phone }
    if (comment) {
      params['stoplist_text'] = comment
    }
    return this.api('stoplist/add', params)
  }

  /**
   * Добавление номера в стоплист
   *
   * @param {String} phone
   * @param {String} comment
   *
   * @return {Promise}
   */
  stoplistAdd({phone, comment}) {
    let params = { stoplist_phone: phone }
    if (comment) {
      params['stoplist_text'] = comment
    }
    return this.api('stoplist/add', params)
  }

  /**
   * Удаление номера из стоплиста
   *
   * @param {String} phone
   *
   * @return {Promise}
   */
  stoplistDel(phone) {
    const params = { stoplist_phone: phone }
    return this.api('stoplist/del', params)
  }

  /**
   * Получение номеров стоплиста
   *
   * @return {Promise}
   */
  stoplistGet() {
    return this.api('stoplist/get')
  }

  /**
   * Добавление callback обработчика
   *
   * @param {String} url
   *
   * @return {Promise}
   */
  callbackAdd(url) {
    return this.api('callback/add', { url })
  }

  /**
   * Удаление callback обработчика
   *
   * @param {String} url
   *
   * @return {Promise}
   */
  callbackDel(url) {
    return this.api('callback/del', { url })
  }

  /**
   * Получение списка callback обработчиков
   *
   * @return {Promise}
   */
  callbackGet() {
    return this.api('callback/get')
  }

  /**
   * Авторизация пользователя по звонку:
   * Передача номера телефона пользователя
   *
   * @param {String} phone
   *
   * @return {Promise}
   */
  callcheckAdd(phone) {
    return this.api('callcheck/add', { phone })
  }

  /**
   * Авторизация пользователя по звонку:
   * Проверка статуса звонка
   *
   * @param {String} checkId
   *
   * @return {Promise}
   */
  callcheckStatus(checkId) {
    return this.api('callcheck/status', { check_id: checkId })
  }

  /**
   * Запрос к серверу API
   *
   * @param {String} method
   * @param {Object} params
   *
   * @return {Promise}
   */
  api(method, params) {
    if (!params) {
      params = {}
    }

    if (this.apiId) {
      params['api_id'] = this.apiId
    } else {
      params['login'] = this.login
      params['password'] = this.password
    }

    params['json'] = 1

    const requestUrl = `${API_URL}${method}?${qs.stringify(params)}`

    const promise = new Promise((resolve, reject) => {
      // resolve(requestUrl)
      https
        .get(requestUrl, (response) => {
          response.setEncoding('utf8')
          let rawData = ''
          response
            .on('data', (chunk) => rawData += chunk)
            .on('end', () => {
              try {
                let data = JSON.parse(rawData)
                if (data.status == 'ERROR') return reject(new Error(data.status_text))
                resolve(data)
              } catch (err) {
                reject(err)
              }
            })
        })
        .on('error', reject)
    })

    return promise
  }

}

module.exports = SMSru
